# Cratable

An extended table module for Cratera

## Examples

Simple use-case:

```lua
local t = table()
t:[table].insert("Hello")
t:[table].insert("World!")
print(t:[table].concat(", ")) --> Hello, World!
```

Advanced use-case:

```lua
local function works_on_table_likes(t)
  t:[table].insert("Hello")
  t:[table].insert("World!")
end

local t = {n=0}
t[table] = {
  insert=function(t,...)
    table.insert(t,...)
    t.n = t.n + 1
  end,
  concat=function(t,s,i,j)
    return table.concat(t,s,i or 1, j or t.n)
  end
}

works_on_table_likes(t)

print(t:[table].concat(", ")) --> Hello, World!
print(t.n) --> 2
```

## License

```
Cratable - Extended table module for Cratera
Copyright (c) 2017 Soni L.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
```
